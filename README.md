![Caltrain Live logo](https://i.imgur.com/T0PHvch.png)

This repository contains the source code for the Caltrain Live realtime data server. Its purpose is to regularly and frequently fetch data from realtime data sources, including 511.org, etc. and compile them into servable data for the Caltrain Live client app. The server must fetch data in the background, even if no client requests are being made, to ensure data is fresh, available, and fast to serve. The server is written in JavaScript and runs on the [Cloudflare Workers](https://workers.cloudflare.com/) platform.

While browsing the Caltrain Live codebase, you may notice that communications between the client app and realtime data server utilize `AliceSprings` headers. The `AliceSprings` name refers to the Caltrain Live realtime data system in general, including the both the server and client-server communications. Its origin is arbitrary and an artifact of the system used for assigning codenames internally for jottocraft projects and systems. Caltrain Live realtime data utilizes the `AliceSprings-useragent` header in the format `AliceSprings/X.Y`, with `X.Y` referring to a special compatibility version indicating to the realtime server which format of realtime data & timetable updates it can accept. 

### Resources

- [Source code](https://gitlab.com/jottocraft/train)
- [Main website](https://train.jottocraft.com)
- [App on Google Play](https://play.google.com/store/apps/details?id=com.jottocraft.train)