// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

declare type GTFSHeader = {
    Timestamp: number;
};

declare type TextTranslation = {
    Text: string;
    Language: string;
};

declare type ActivePeriod511 = {
    Start?: number;
    End?: number;
};

declare type ActivePeriodCaltrain = {
    Start?: string;
    End?: string;
};

declare type ActivePeriod = ActivePeriod511 | ActivePeriodCaltrain;

declare type Alert511 = {
    ActivePeriods: ActivePeriod511[];
    HeaderText: {
        Translations: TextTranslation[];
    };
    DescriptionText: {
        Translations: TextTranslation[];
    };
};

declare type AlertEntity511 = {
    Id: string;
    Alert: Alert511;
};

declare type InformedEntityCaltrain = {
    AgencyId: "CT";
    StopId?: string;
};

declare type AlertCaltrain = {
    ActivePeriod: ActivePeriodCaltrain;
    HeaderText: {
        Translation: TextTranslation[];
    };
    DescriptionText: {
        Translation: TextTranslation[];
    };
    InformedEntity: InformedEntityCaltrain[];
};

declare type AlertEntityCaltrain = {
    Id: number;
    Alert: AlertCaltrain;
    Source: "PADS";
};

declare type Alert = Alert511 | AlertCaltrain;

declare type AlertEntity = AlertEntity511 | AlertEntityCaltrain;

declare type AlertsResponse = {
    Header: GTFSHeader;
    Entities: AlertEntity[];
};

declare type StopTimeUpdate511 = {
    StopId: string;
    Arrival?: {
        Time: number;
    };
    Departure?: {
        Time: number;
    };
};

declare type TripUpdate511 = {
    StopTimeUpdates: StopTimeUpdate511[];
};

declare type TripUpdateEntity511 = {
    Id: string;
    TripUpdate: TripUpdate511;
};