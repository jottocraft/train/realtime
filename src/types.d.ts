// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

declare type RealtimeEnv = {
    TIMETABLE: KVNamespace;
    HTTP_STATUS_PAGES: Fetcher;
    STATS: AnalyticsEngineDataset;
    API_KEY: string;
    LAYER: DurableObjectNamespace;
    DEV: "TRUE" | "FALSE";
    LAYER_ID: string;
};

declare type RealtimeDevice = {
    timetable?: number;
    clock?: number;
    clockOffset?: number;
    iid?: string;
    ua?: RealtimeUserAgent;
    platform?: string;
    runtime?: string;
    ota?: string;
    isp?: string;
};

declare type RealtimeUserAgent = {
    str: string;
    syn: boolean;
};

declare type AsErrorMessage = {
    title: string;
    text: string;
};

declare type LiveassetHeader = {
    name: string;
    ver: number;
    id: string;
    type: "bundle" | "backend";
    ua: string;
};

declare type RealtimeAlert = {
    title: string | null;
    text: string;
    date?: string;
    id?: string;
    from: string;
    affectsDates?: {
        from?: string;
        to?: string;
        dates?: string[];
    };
    for?: string[];
};

declare type RealtimeTrips = {
    [trainNum: string]: {
        stops: {
            [stopNum: string]: {
                arr: number | null;
                dep: number | null;
            };
        };
    };
};

declare type RealtimePatch = {
    trips: RealtimeTrips;
    alerts: RealtimeAlerts[];
    ao: number;
    hasFailures?: boolean;
};