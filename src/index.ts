// 
//   ____      _ _             _         ____  _        _
//  / ___|__ _| | |_ _ __ __ _(_)_ __   / ___|| |_ __ _| |_ _   _ ___
// | |   / _` | | __| '__/ _` | | '_ \  \___ \| __/ _` | __| | | / __|
// | |__| (_| | | |_| | | (_| | | | | |  ___) | || (_| | |_| |_| \__ \
//  \____\__,_|_|\__|_|  \__,_|_|_| |_| |____/ \__\__,_|\__|\__,_|___/
// 
//  ___  ___ _ ____   _____ _ __
// / __|/ _ \ '__\ \ / / _ \ '__|
// \__ \  __/ |   \ V /  __/ |
// |___/\___|_|    \_/ \___|_|
// 
// Copyright (c) jottocraft 2018

import asError from "~/asError";
import { commonResHeaders, PREV_ALLOWABLE_UAs } from "~/common";
import uaOutOfDate from "~/uaOutOfDate";

// Handle realtime API request
async function serveRealtime(request: Request, env: RealtimeEnv, ctx: ExecutionContext, device: RealtimeDevice) {
	const url = new URL(request.url);
	const viaHq = url.hostname.endsWith(".hq.jottocraft.com");

	if (request.method === "OPTIONS") {
		return new Response(null, {
			status: 204,
			headers: commonResHeaders
		});
	}

	const layer = env.LAYER.get(env.LAYER.idFromString(env.LAYER_ID));

	if (viaHq) {
		if (url.pathname === "/api/transit/realtime/alarm") return layer.fetch("https://RealtimeLayer.do.jottocraft.com/alarm");
		if (url.pathname === "/api/transit/realtime/alarm/start") return layer.fetch("https://RealtimeLayer.do.jottocraft.com/alarm/start");
		if (url.pathname === "/api/transit/realtime/alarm/stop") return layer.fetch("https://RealtimeLayer.do.jottocraft.com/alarm/stop");
		if (url.pathname === "/api/transit/realtime/patchData/clear") return layer.fetch("https://RealtimeLayer.do.jottocraft.com/patchData/clear");	
	}

	if (!device.ua?.str || !device.ua.str.startsWith("AliceSprings/")) {
		return env.HTTP_STATUS_PAGES.fetch("https://http-status-pages.jottocraft.com/?status=401&brand=train");
	}

	const [
		patchData,
		liveasssetStr
	] = await Promise.all([
		layer.fetch("https://RealtimeLayer.do.jottocraft.com/patchData").then(r => r.text()),
		env.TIMETABLE.get("liveasset")
	]);

	if (!liveasssetStr) {
		return asError({
			title: "Server error",
			text: "The Caltrain Live server was unable to load the latest timetable data. Please try again later."
		});
	}

	const liveasset: LiveassetHeader = JSON.parse(liveasssetStr);

	if ((device.ua.str !== liveasset.ua) && !device.ua.syn && !PREV_ALLOWABLE_UAs.includes(device.ua.str)) {
		return uaOutOfDate(device.ua.str);
	}

	if (!patchData) {
		return asError({
			title: "Server error",
			text: "The Caltrain Live realtime data server doesn't have any valid updates to serve. Please try again later."
		});
	}

	if ((device.clockOffset !== undefined) && (device.clockOffset > (5 * 60 * 1000))) {
		return asError({
			title: "Out of sync",
			text: "Realtime data is unavailable to sync with your device because your clock is more than 5 minutes off."
		});
	}

	if ((device.ua.str === liveasset.ua) && device.timetable && (device.timetable < liveasset.ver)) {
		// UA must exactly match timetable UA to get updates
		const serveableTimetable: unknown = await env.TIMETABLE.get("timetable");
		return new Response(`{"liveasset":${serveableTimetable},"patch":${patchData}}`, {
			status: 200,
			headers: {
				...commonResHeaders,
				"Content-Type": "application/json",
				"AliceSprings-Response": "liveassetPatch"
			}
		})
	}

	return new Response(patchData, {
		status: 200,
		headers: {
			...commonResHeaders,
			"Content-Type": "application/json",
			"AliceSprings-Response": "patch"
		}
	});
}

export default {
	async fetch(request: Request, env: RealtimeEnv, ctx: ExecutionContext) {
		const UA = request.headers.get("AliceSprings-useragent") || request.headers.get("User-Agent");
		const isSynUA = UA === "AliceSprings/Syn";

		const asClock = request.headers.get("AliceSprings-Clock");
		const asLiveasset = request.headers.get("AliceSprings-Liveasset");

		const device: RealtimeDevice = {
			timetable: asLiveasset ? Number(asLiveasset) : undefined,
			clock: asClock ? Number(asClock) : undefined,
			clockOffset: asClock ? Math.abs(Number(asClock) - new Date().getTime()) : undefined,
			iid: request.headers.get("AliceSprings-IID") ?? undefined,
			ua: UA ? {
				str: UA,
				syn: isSynUA
			} : undefined,
			platform: request.headers.get("AliceSprings-platform") ?? undefined,
			runtime: request.headers.get("AliceSprings-runtime") ?? undefined,
			ota: request.headers.get("AliceSprings-ota") ?? undefined,
			isp: (request.cf?.asOrganization as string) ?? undefined
		};

		const res = await serveRealtime(request, env, ctx, device);

		ctx.waitUntil((async function () {
			if (!device.ua || device.ua.syn || !device.timetable || !device.iid || (device.clockOffset === undefined) || !env.STATS) return;

			// Record analytics
			env.STATS.writeDataPoint({
				blobs: [
					device.ua.str,
					String(device.timetable),
					res.headers.get("AliceSprings-Response"),
					String(res.status),
					device.platform ?? "unknown",
					device.runtime ?? "unknown",
					device.ota ?? "unknown",
					device.isp ?? "unknown"
				],
				doubles: [
					device.clockOffset
				],
				indexes: [device.iid.replaceAll("-", "")]
			});
		})());

		return res;
	}
};

export { RealtimeLayer } from "~/layer";
