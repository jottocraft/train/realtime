// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import commonHeaders from "~/layer/commonHeaders";
import parseGTFSAlert from "~/layer/parseGTFSAlert";

export default async function getCaltrainAlerts() {
    try {
        const caltrainAlerts: AlertEntityCaltrain[] = await (
            fetch("https://www.caltrain.com/gtfs/api/v1/servicealerts/3655", {
                headers: commonHeaders
            }).then(r => r.json())
        );

        const compiledAlerts: RealtimeAlert[] = [];

        // Process Caltrain alerts
        if (caltrainAlerts) {
            caltrainAlerts.forEach(alert => {
                // Filter out non-Caltrain alerts
                if (alert.Source !== "PADS") return;

                const formattedAlert = parseGTFSAlert(alert);

                if (formattedAlert) {
                    const realtimeAlert: RealtimeAlert = {
                        ...formattedAlert,
                        text: formattedAlert.text?.replaceAll("\n", " "),
                        from: "PADS"
                    };

                    const forStations = alert.Alert.InformedEntity.map(entity => entity.StopId).filter(stopID => Boolean(stopID)) as string[];
                    if (forStations.length && (forStations.length < 24)) realtimeAlert.for = forStations;

                    compiledAlerts.push(realtimeAlert);
                }
            });
        }

        return { data: compiledAlerts };
    } catch (e) {
        // Silently fail here because this API isn't supported or stable
        return { data: [] };
    }
}
