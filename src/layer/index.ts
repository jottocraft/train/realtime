// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import get511Alerts from "~/layer/get511Alerts";
import getCaltrainAlerts from "~/layer/getCaltrainAlerts";
import getTripStatuses from "~/layer/getTripStatuses";

export class RealtimeLayer implements DurableObject {
    private env: RealtimeEnv;
    private state: DurableObjectState;
    private storage: DurableObjectStorage;

    constructor(state: DurableObjectState, env: RealtimeEnv) {
        this.env = env;
        this.state = state;
        this.storage = state.storage;
    }

    async alarm() {
        console.log("Running RealtimeLayer alarm");

        const prevData = await this.storage.get<RealtimePatch | undefined>("patch");
        const patch = await buildRealtimePatch(this.env, prevData);
        await this.storage.put<RealtimePatch>("patch", patch);

        const nextAlarm = new Date(new Date().getTime() + (1000 * 30));
        console.log("Finished processing fresh realtime data... next alarm at " + nextAlarm.toLocaleTimeString("en-US") + " UTC");

        // Refresh data every 30 seconds
        return this.storage.setAlarm(nextAlarm);
    }

    async verifyAlarmSet(patch?: RealtimePatch) {
        // No need to check alarm storage if realtime data is available and within 3 minutes old
        if (patch && (Math.abs(patch.ao - new Date().getTime()) < (1000 * 60 * 3))) return;

        const alarm = await this.storage.getAlarm();
        if (alarm === null) {
            // Start realtime alarm cycle
            this.state.blockConcurrencyWhile(async () => {
                console.log("Blocking concurrency to start alarm cycle");
                await this.alarm();
            });
        }
    }

    async fetch(request: Request): Promise<Response> {
        const url = new URL(request.url);

        if (url.pathname === "/patchData/clear") {
            await this.storage.delete("patch");
            return new Response(null, { status: 204 });
        } else if (url.pathname === "/alarm") {
            return new Response(String(await this.storage.getAlarm()), { status: 200 });
        } else if ((this.env.DEV === "TRUE") && (url.pathname === "/alarm/start")) {
            await this.alarm();
            return new Response(null, { status: 204 });
        } else if ((this.env.DEV === "TRUE") && (url.pathname === "/alarm/stop")) {
            await this.storage.deleteAlarm();
            return new Response(null, { status: 204 });
        }

        const patch = await this.storage.get<RealtimePatch | undefined>("patch");

        if (this.env.DEV !== "TRUE") {
            this.state.waitUntil(this.verifyAlarmSet(patch));
        }

        if (patch === undefined) {
            return new Response(null, { status: 500 });
        }

        return new Response(JSON.stringify(patch), {
            status: 200,
            headers: {
                "Content-Type": "application/json"
            }
        });
    }
}

// Get some fresh never frozen data
async function buildRealtimePatch(env: RealtimeEnv, prevData?: RealtimePatch): Promise<RealtimePatch> {
    const processingAlerts: RealtimeAlert[] = [];

    async function withErrorCatcher<T>(promise: Promise<{ data: T; processingAlerts?: RealtimeAlert[]; }>, emptyValue: T): Promise<T> {
        try {
            const data = await promise;

            if (data.processingAlerts?.length) {
                processingAlerts.push(...data.processingAlerts);
            }

            return data.data;
        } catch (e) {
            console.log("Caught processing error", e instanceof Error ? e.stack : e);

            processingAlerts.push({
                title: "Uncaught Realtime Data Server Error",
                text: "An uncaught server error occurred when processing realtime updates. Some data may be unavailable; please check status.jottocraft.com for updates.",
                from: "jottocraft"
            });

            return emptyValue;
        }
    }

    const [
        trips,
        alerts511,
        alertsCaltrain
    ] = await Promise.all([
        withErrorCatcher(getTripStatuses(env, prevData), {}),
        withErrorCatcher(get511Alerts(env), []),
        withErrorCatcher(getCaltrainAlerts(), [])
    ]);

    //Construct response object
    const patchData: RealtimePatch = {
        trips,
        alerts: [
            ...processingAlerts,
            ...alerts511,
            ...alertsCaltrain
        ].sort((a, b) => (new Date(b.date ?? 0).getTime() - new Date(a.date ?? 0).getTime())),
        ao: new Date().getTime(),
        hasFailures: processingAlerts.length > 0
    };

    //Process jottocraft alerts
    if (false) {
        patchData.alerts.unshift({
            title: "Caltrain Live Reduced Weekend Service Unavailable",
            text: "Caltrain Live is not able to show trains during reduced bus bridge weekend service on Aug 19-20 and 26-27. As of August 15, 2023, these trains are not included in Caltrain's published GTFS data. Please check caltrain.com/status and plan ahead for these dates. Thank you for your patience and understanding.",
            id: "AliceSprings/2023-08/WeekendServiceOutage",
            from: "jottocraft",
            affectsDates: {
                dates: ["2023-08-19", "2023-08-20", "2023-08-26", "2023-08-27"]
            }
        });
    }

    console.log("built patch");

    return patchData;
}
