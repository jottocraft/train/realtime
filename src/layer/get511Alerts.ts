// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import commonHeaders from "~/layer/commonHeaders";
import parseGTFSAlert from "~/layer/parseGTFSAlert";

export default async function get511Alerts(env: RealtimeEnv): Promise<{ data: RealtimeAlert[]; processingAlerts?: RealtimeAlert[]; }> {
    const serviceAlertsResponse = await fetch("https://api.511.org/Transit/ServiceAlerts?format=JSON&agency=CT&api_key=" + env.API_KEY, {
        headers: commonHeaders
    });

    if (serviceAlertsResponse.status >= 500) {
        return {
            data: [],
            processingAlerts: [{
                title: "Upstream Realtime Data Error",
                text: "Service alerts are unavailable due to an error in the 511.org API.",
                from: "jottocraft"
            }]
        };
    } else if (serviceAlertsResponse.status !== 200) {
        return {
            data: [],
            processingAlerts: [{
                title: "Realtime Data Server Error",
                text: "An error occurred when requesting service alert data from the 511.org API.",
                from: "jottocraft"
            }]
        };
    }

    const serviceAlerts: {
        Entities: AlertEntity511[];
    } = await serviceAlertsResponse.text().then(t => JSON.parse(t.substring(1)));

    const compiledAlerts: RealtimeAlert[] = [];

    //Process 511 alerts
    if (serviceAlerts.Entities) {
        serviceAlerts.Entities.forEach(alert => {
            const formattedAlert = parseGTFSAlert(alert);

            if (formattedAlert) {
                //Add alert
                compiledAlerts.push({
                    ...formattedAlert,
                    from: "511"
                });
            }
        });
    }

    return { data: compiledAlerts };
}
