// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import commonHeaders from "~/layer/commonHeaders";

const TWELVE_HOURS = 12 * 60 * 60 * 1000;

export default async function getTripStatuses(env: RealtimeEnv, prevData?: RealtimePatch): Promise<{ data: RealtimeTrips; processingAlerts?: RealtimeAlert[]; }> {
    const tripUpdatesResponse = await fetch("https://api.511.org/Transit/TripUpdates?format=JSON&agency=CT&api_key=" + env.API_KEY, {
        headers: commonHeaders
    });

    if (tripUpdatesResponse.status >= 500) {
        return {
            data: {},
            processingAlerts: [{
                title: "Upstream Realtime Data Error",
                text: "Train delay data is unavailable due to an error in the 511.org API.",
                from: "jottocraft"
            }]
        };
    } else if (tripUpdatesResponse.status !== 200) {
        return {
            data: {},
            processingAlerts: [{
                title: "Realtime Data Server Error",
                text: "An error occurred when requesting train delay data from the 511.org API.",
                from: "jottocraft"
            }]
        };
    }

    const tripUpdates: {
        Entities: TripUpdateEntity511[];
    } = await tripUpdatesResponse.text().then(t => JSON.parse(t.substring(1)));

    const trips: RealtimeTrips = {};
    const processingAlerts = [];

    // Process realtime updates
    let caughtStaleData = false;
    if (tripUpdates.Entities) {
        const now = new Date().getTime();
        tripsLoop: for (const train of tripUpdates.Entities) {
            trips[train.Id] = prevData?.trips[train.Id] ?? { stops: {} };

            if (!train.TripUpdate?.StopTimeUpdates) continue;

            for (const stop of train.TripUpdate.StopTimeUpdates) {
                if (!stop.Arrival?.Time && !stop.Departure?.Time) continue;
                const timing = {
                    arr: stop.Arrival?.Time ? stop.Arrival.Time * 1000 : stop.Departure!.Time * 1000,
                    dep: stop.Departure?.Time ? stop.Departure.Time * 1000 : stop.Arrival!.Time * 1000
                };

                // Sometimes 511 data freezes up and serves old data, catch that here
                if ((Math.abs(now - timing.arr) > TWELVE_HOURS) || (Math.abs(now - timing.dep) > TWELVE_HOURS)) {
                    caughtStaleData = true;
                    delete trips[train.Id];
                    continue tripsLoop;
                }

                trips[train.Id].stops[stop.StopId] = timing;
            }
        }
    }

    if (caughtStaleData) {
        processingAlerts.push({
            title: "Upstream Realtime Data Error",
            text: "The 511.org API is returning some data that's over 12 hours old. Realtime train delays may be unavailable.",
            from: "jottocraft"
        });
    }

    return { data: trips, processingAlerts };
}
