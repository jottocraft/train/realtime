// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

function getTranslationsText(translations: TextTranslation[]) {
    if (!translations) return null;
    return translations.find(t => t.Language == "en")?.Text.replace(/(<([^>]+)>)/ig, '') || null;
}

export default function parseGTFSAlert(entity: AlertEntity): Omit<RealtimeAlert, "from"> | null {
    const alert = entity.Alert;

    const ActivePeriods = "ActivePeriods" in alert ? alert.ActivePeriods : [alert.ActivePeriod];
    const HeaderTranslations = "Translations" in alert.HeaderText ? alert.HeaderText.Translations : alert.HeaderText.Translation;
    const DescriptionTranslations = "Translations" in alert.DescriptionText ? alert.DescriptionText.Translations : alert.DescriptionText.Translation;

    const now = new Date().getTime();
    let alertCurrentStart: number | undefined;
    function isInActivePeriod(p: ActivePeriod) {
        const startTime = p.Start !== undefined ? Number(p.Start) * 1000 : undefined, 
        endTime = p.End !== undefined ? Number(p.End) * 1000 : undefined;

        const isAfterStart = startTime !== undefined ? startTime < now : true;
        if ((startTime !== undefined) && isAfterStart) {
            if ((alertCurrentStart === undefined) || (startTime > alertCurrentStart)) alertCurrentStart = startTime;
        }

        const isBeforeEnd = endTime !== undefined ? endTime > now : true;

        return isAfterStart && isBeforeEnd;
    }

    // Check active periods
    let alertIsActive = false;
    for (let i = 0; i < ActivePeriods.length; i++) {
        if (isInActivePeriod(ActivePeriods[i])) {
            alertIsActive = true;
        }
    }

    if (!alertIsActive) return null;

    const title = getTranslationsText(HeaderTranslations);
    const text = getTranslationsText(DescriptionTranslations);

    if (!text) return null;

    return {
        title,
        text,
        date: alertCurrentStart ? new Date(alertCurrentStart).toISOString() : undefined,
        id: String(entity.Id)
    };
}
