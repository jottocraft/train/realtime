// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import commonResHeaders from "~/layer/commonHeaders";

export default function asError(msg: AsErrorMessage): Response {
    return new Response(JSON.stringify(msg), {
        status: 400,
        headers: {
            ...commonResHeaders,
            "Content-Type": "application/json",
            "AliceSprings-Response": "error"
        }
    });
}
