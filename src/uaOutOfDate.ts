// Caltrain Live Realtime Data Server
// Copyright (c) jottocraft 2023

import asError from "~/asError";
import { commonResHeaders } from "~/common";

//Send a user-agent out of date error
export default function uaOutOfDate(ua: string): Response {
    if (ua.startsWith("AliceSprings/5") || ua.startsWith("AliceSprings/8")) {
        return asError({
            title: "Please update Caltrain Live",
            text: "Please update to the latest version of Caltrain Live on Google Play to get realtime data and the latest timetable updates."
        });
    }

    return new Response(JSON.stringify({
        Siri: {},
        error: "errorMsg~Please update Caltrain LiveerrorMsg~Please update to the latest version of Caltrain Live on Google Play to get realtime data and the latest timetable updates.",
        alertData: []
    }), {
        status: 200,
        headers: {
            ...commonResHeaders,
            "Content-Type": "application/json"
        }
    });
}
